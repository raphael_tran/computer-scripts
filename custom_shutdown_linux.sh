#!/bin/bash

shutdown -c;	# Canceling a potential shutdown already set up


echo "Minutes before shutdown [0]: ";
read delay_min;

clear

# If nothing is typed
if [ -z $delay_min ]; then	
	shutdown now;
	exit 0;
fi


echo;
shutdown +$delay_min;


while true; do
	read;
done;

# To do : add a timer instead of the infinite loop
# Ctrl+C cancel the shutdown

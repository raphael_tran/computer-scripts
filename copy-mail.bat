:: This script puts an email adress in the clipboard.
:: You can then create a shortcut to it on the desktop
:: and set a keyboard shortcut to run it.

@echo off
echo.

rem set the curr variable to the content of the clipboard, with the invalid character "" in front (no idea why)
for /f "delims=" %%a in ('powershell Get-Clipboard') do @set curr=%%a


rem /!\ delete the special character below if it doesn't work /!\
if NOT "%curr%" == "main@email.com" (
	echo|set/p=main@email.com| clip
	echo "main@email.com" in clipboard.
) else (
	echo|set/p=second.email@work.eu| clip
	echo "second.email@work.eu" in clipboard.
)

timeout /t 2 > NUL
exit

@echo off
:: This script restart the computer after asking a confirmation

echo.
echo Press Enter to confirm restart...
runas /user:# "" >nul 2>&1

shutdown -r -t 0
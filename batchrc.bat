﻿::This is run at the startup of the cmd because there is an Autorun register key located at: HKEY_CURRENT_USER\Software\Microsoft\Command Processor
cls
@echo off

:: UTF-8
chcp 65001 > NUL

:: Allow a quick clear with L->Return
doskey l=cls

:: Setting up aliases
"C:\Config\Console\aliases\_aliases.bat"
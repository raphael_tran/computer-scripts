@echo off
:: This script enables to prompt an input to set the shutdown delay

chcp 65001 > NUL

:: "Test" shutdown with a large time to determine whether there was a shutdown pending
shutdown -s -t 999999

:: If there is already a shutdown pending then %ERRORLEVEL% will be 1190
if %ERRORLEVEL% equ 1190 (
	echo Scheduled shutdown cancelled.
)

:Start
:: Cancelling potential previous shutdown or "test" shutdown
shutdown -a


echo.
set /p delay_min="Minutes before shutdown [0]: "

:: if the user just pressed Enter
if not defined delay_min (
	:: Shuting down immediately
	shutdown -s -t 0
	exit
)

:: Converting minutes to seconds
set /a "delay_sec = delay_min * 60"

cls
echo.
echo Shutdown initiated at: %TIME%
shutdown -s -t %delay_sec%
echo with a delay of: %delay_min% minutes

echo.
echo.

rem to keep the console open or cancel the shutdown
timeout %delay_sec%

echo Press Enter to confirm cancellation...
runas /user:# "" >nul 2>&1


:: Cancelling the shutdown
shutdown -a
echo.
cls
echo Shutdown cancelled.
timeout 3 > NUL
